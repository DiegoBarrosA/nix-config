{ inputs, pkgs, ... }: 
{
  imports = [
    ./global
    ./features/cli
    ./features/desktop/common/font.nix
    ./features/global/darwin.nix
    ./features/desktop/common/wayland-wm/alacritty.nix

  ];
  home.packages = [ pkgs.cascadia-code ];
  colorscheme = inputs.nix-colors.colorSchemes.material-darker;
}
