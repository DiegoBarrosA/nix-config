{ pkgs, ... }:
{
  services.mopidy = {
    enable = true;
    dataDir = "/storage/var/lib/mopidy";
    extensionPackages = with pkgs; [
      mopidy-mpd
      mopidy-mpris
mopidy-musicbox-webclient
mopidy-youtube
    ];
    configuration = ''
[mpd]
hostname = ::
enabled = true
 '';
  };
}
