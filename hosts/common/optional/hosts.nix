{ config, pkgs, fetchurl, ... }:

let
  extrahostsfromsteve = pkgs.fetchurl {
    url =
      "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts";
    sha256 = "fSAu4rp8snOm05be47E5mU0b0VtjEW0Js032uVupZ44=";
  };
in { networking.extraHosts = "${builtins.readFile extrahostsfromsteve} "; }
